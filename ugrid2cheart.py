################################################################################
# ugrid2cheart.py
#
# converts a *.ugrid file to cheart X/T/B files
#
# author: Andreas Hessenthaler (andreas.hessenthaler@gmail.com)
#
# last modified: 25/11/2015
#
# usage: python ugrid2cheart.py
#
# requires: python, numpy, cython
#
# requires also: findPatchLabels.so (you can re-build it using the Makefile if the present shared object doesn't work on your platform)
#
# implemented for tet-meshes, that may also contain pyramid elements
#                             --> these are split into tets
#
import numpy
import findPatchLabels

################################################################################
############## PARAMETERS ######################################################
################################################################################
# filename - import
fin   = "ventricle-atrium.ugrid"

# filename - export
foutX = "domainF_lin_FE.X"
foutT = "domainF_lin_FE.T"
foutB = "domainF_lin_FE.B"

# coordinates are multiplied by this variable
# e.g. if your model is in meter, but you need it in millimeter
scale = 1.0

# ugrid labels (i.e. face number/identifier as shown in Simmetrix/SimModeler)
# user '13131313131313' if there is no face assigned to specific boundary condition
patch1              = numpy.array([1],             dtype=numpy.int)
patch2              = numpy.array([2],             dtype=numpy.int)
patch3              = numpy.array([3],             dtype=numpy.int)
patch4              = numpy.array([4],             dtype=numpy.int)
patch5              = numpy.array([5],             dtype=numpy.int)
patch6              = numpy.array([6],             dtype=numpy.int)
patch7              = numpy.array([7],             dtype=numpy.int)
patch8              = numpy.array([8],             dtype=numpy.int)

# cheart patch labels
patchID1        = 1
patchID2        = 2
patchID3        = 3
patchID4        = 4
patchID5        = 5
patchID6        = 6
patchID7        = 7
patchID8        = 8
################################################################################
################################################################################
################################################################################


finHandle = open(fin, "r")
firstLine = finHandle.readline()
temp = firstLine.split()
numNodes = int(temp[0])
numTris  = int(temp[1])
numQuads = int(temp[2])
numTets  = int(temp[3])
numPys   = int(temp[4])
numWedgs = int(temp[5])
numHexs  = int(temp[6])
numElems = numTets + 2*numPys + numWedgs + numHexs


print "write X file"
foutXHandle = open(foutX, "w")
foutXHandle.write("%i 3\n" % (numNodes))
foutXHandle.close()
with open(foutX, "a") as foutXHandle:
    for i in range(numNodes):
        temp = finHandle.readline().split()
        c0 = float(temp[0]) * scale
        c1 = float(temp[1]) * scale
        c2 = float(temp[2]) * scale
        foutXHandle.write("% .16f % .16f % .16f\n" % (c0, c1, c2))

# get TRIANGLES
tris = numpy.zeros((numTris+2*numQuads, 3), dtype=numpy.int)
for i in range(numTris):
    temp = finHandle.readline().split()
    tris[i, 0] = int(temp[0])
    tris[i, 1] = int(temp[1])
    tris[i, 2] = int(temp[2])

# get QUADRILATERALS as TRIANGLES
for i in range(numQuads):
    temp = finHandle.readline().split()
    tris[numTris+i*2, 0]   = int(temp[0])
    tris[numTris+i*2, 1]   = int(temp[1])
    tris[numTris+i*2, 2]   = int(temp[3])
    tris[numTris+i*2+1, 0] = int(temp[3])
    tris[numTris+i*2+1, 1] = int(temp[1])
    tris[numTris+i*2+1, 2] = int(temp[2])

# get TRIANGLE face number
facenr = numpy.zeros((numTris+2*numQuads, ), dtype=numpy.int)
for i in range(numTris):
    facenr[i] = int(finHandle.readline())

# get QUADRILATERAL face number
for i in range(numQuads):
    temp = finHandle.readline().split()
    facenr[numTris+i*2]   = int(temp[0])
    facenr[numTris+i*2+1] = int(temp[0])


print "write T file"# (tets)
foutTHandle = open(foutT, "w")
foutTHandle.write("%i %i\n" % (numElems, numNodes))
foutTHandle.close()
tets = numpy.zeros((numTets+2*numPys, 4), dtype=numpy.int)
with open(foutT, "a") as foutTHandle:
    # tets
    for i in range(numTets):
        temp = finHandle.readline().split()
        tets[i, 0]   = int(temp[0])
        tets[i, 1]   = int(temp[1])
        tets[i, 2]   = int(temp[2])
        tets[i, 3]   = int(temp[3])
        foutTHandle.write("%16i %16i %16i %16i\n" % (tets[i, 0], tets[i, 1], tets[i, 2], tets[i, 3]))
    # convert pys to tets
    for i in range(numPys):
        temp = finHandle.readline().split()
        tets[numTets+i*2, 0]   = int(temp[0])
        tets[numTets+i*2, 1]   = int(temp[1])
        tets[numTets+i*2, 2]   = int(temp[2])
        tets[numTets+i*2, 3]   = int(temp[3])
        tets[numTets+i*2+1, 0]   = int(temp[1])
        tets[numTets+i*2+1, 1]   = int(temp[3])
        tets[numTets+i*2+1, 2]   = int(temp[2])
        tets[numTets+i*2+1, 3]   = int(temp[4])
        foutTHandle.write("%16i %16i %16i %16i\n" % (tets[numTets+i*2, 0], tets[numTets+i*2, 1], tets[numTets+i*2, 2], tets[numTets+i*2, 3]))
        foutTHandle.write("%16i %16i %16i %16i\n" % (tets[numTets+i*2+1, 0], tets[numTets+i*2+1, 1], tets[numTets+i*2+1, 2], tets[numTets+i*2+1, 3]))


# export WEDGES
for i in range(numWedgs):
    finHandle.readline()


# export HEXAHEDRA
for i in range(numHexs):
    finHandle.readline()


print "write B file"# (tets)
foutBHandle = open(foutB, "w")
foutBHandle.write("%i\n" % (numTris+2*numQuads))
foutBHandle.close()
elementNumbers, cheartLabels = findPatchLabels.labels(facenr, tris, tets, \
    patchID1,   patchID2,   patchID3,   patchID4,   patchID5,   patchID6,   patchID7,   patchID8, \
    patch1,     patch2,     patch3,     patch4,     patch5,     patch6,     patch7,     patch8)

with open(foutB, "a") as foutBHandle:
    for i in range(numTris+2*numQuads):
        n0 = tris[i, 0]
        n1 = tris[i, 1]
        n2 = tris[i, 2]
        foutBHandle.write("%16i %16i %16i %16i %2i\n" % (elementNumbers[i], n0, n1, n2, cheartLabels[i]))
        print "\r{0} % (main loop)".format((float(i)/(numTris+2*numQuads))*100),

finHandle.close()
