import numpy
import string
import sys
cimport numpy
cimport cython
from numpy cimport ndarray
from libc.stdio cimport *
from libc.math cimport sqrt
from libc.math cimport abs
from libc.math cimport pow

sys.dont_write_bytecode = True


def labels(ndarray[numpy.int_t, ndim=1] facenr, ndarray[numpy.int_t, ndim=2] tris, ndarray[numpy.int_t, ndim=2] tets, \
           int inlet, int outlet, int slipwall, int wall, int interface, \
           int patch6, int patch7, int patch8, \
           ndarray[numpy.int_t, ndim=1] inletfacenr, \
           ndarray[numpy.int_t, ndim=1] outletfacenr, \
           ndarray[numpy.int_t, ndim=1] slipwallfacenr, \
           ndarray[numpy.int_t, ndim=1] wallfacenr, \
           ndarray[numpy.int_t, ndim=1] interfacefacenr, \
           ndarray[numpy.int_t, ndim=1] patch6nr, \
           ndarray[numpy.int_t, ndim=1] patch7nr, \
           ndarray[numpy.int_t, ndim=1] patch8nr \
          ):
    cdef ndarray[numpy.int_t, ndim=1] cheartLabels
    cdef ndarray[numpy.int_t, ndim=1] elementNumbers
    cdef unsigned int numTets, numTris, count, n0, n1, n2, nr, boolVar, i, j
    
    numTets = tets.shape[0]
    numTris = tris.shape[0]
    cheartLabels   = numpy.zeros(numTris).astype(int)
    elementNumbers = numpy.zeros(numTris).astype(int)
    
    for i in range(numTris):
        n0 = tris[i, 0]
        n1 = tris[i, 1]
        n2 = tris[i, 2]
        for j in range(numTets):
            count = 0
            if tets[j, 0] == n0:
                count += 1
            elif tets[j, 0] == n1:
                count += 1
            elif tets[j, 0] == n2:
                count += 1
            if tets[j, 1] == n0:
                count += 1
            elif tets[j, 1] == n1:
                count += 1
            elif tets[j, 1] == n2:
                count += 1
            if tets[j, 2] == n0:
                count += 1
            elif tets[j, 2] == n1:
                count += 1
            elif tets[j, 2] == n2:
                count += 1
            if tets[j, 3] == n0:
                count += 1
            elif tets[j, 3] == n1:
                count += 1
            elif tets[j, 3] == n2:
                count += 1
            if count >= 3:
                elementNumbers[i] = j+1
                break
        
        nr = facenr[i]
        boolVar = 1
        if boolVar:
            for j in range(inletfacenr.shape[0]):
                if nr == inletfacenr[j]:
                    cheartLabels[i] = inlet
                    boolVar = 0
                    break
        if boolVar:
            for j in range(outletfacenr.shape[0]):
                if nr == outletfacenr[j]:
                    cheartLabels[i] = outlet
                    boolVar = 0
                    break
        if boolVar:
            for j in range(slipwallfacenr.shape[0]):
                if nr == slipwallfacenr[j]:
                    cheartLabels[i] = slipwall
                    boolVar = 0
                    break
        if boolVar:
            for j in range(wallfacenr.shape[0]):
                if nr == wallfacenr[j]:
                    cheartLabels[i] = wall
                    boolVar = 0
                    break
        if boolVar:
            for j in range(patch6nr.shape[0]):
                if nr == patch6nr[j]:
                    cheartLabels[i] = patch6
                    boolVar = 0
                    break
        if boolVar:
            for j in range(patch7nr.shape[0]):
                if nr == patch7nr[j]:
                    cheartLabels[i] = patch7
                    boolVar = 0
                    break
        if boolVar:
            for j in range(patch8nr.shape[0]):
                if nr == patch8nr[j]:
                    cheartLabels[i] = patch8
                    boolVar = 0
                    break
    
    return elementNumbers, cheartLabels
