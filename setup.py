from distutils.core import setup
from Cython.Build import cythonize

setup(ext_modules = cythonize("findPatchLabels.pyx",language="c++",compiler_directives={'profile': False}))
